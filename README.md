compact_vhd
===================

Compacts Hyper-V VHDX Files

**Summary:**

- This script runs with no parameters!
- It will find any VM that is off, and compact the associated hard disk file (VHDX). It lists the VM's it finds before proceeding with the compact.
- It will then find any VM that is on, and ask if you want to save and compact the associated hard disk file (VHDX). It lists the VM's it finds, before proceeding with the save and compact.

**Changelog:**

- Version .02
	- Fixed a bug where the array of hard disks wasn't cleared in between 'off' vm's and 'on' vm's
- Version .01
	- Initial Version
	- Minimal error checking, but it works!