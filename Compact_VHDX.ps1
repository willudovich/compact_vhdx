﻿### BEGIN ELEVEATE TO ADMIN

# Get the ID and security principal of the current user account
$myWindowsID=[System.Security.Principal.WindowsIdentity]::GetCurrent()
$myWindowsPrincipal=new-object System.Security.Principal.WindowsPrincipal($myWindowsID)
 
# Get the security principal for the Administrator role
$adminRole=[System.Security.Principal.WindowsBuiltInRole]::Administrator
 
# Check to see if we are currently running "as Administrator"
if ($myWindowsPrincipal.IsInRole($adminRole))
   {
   # We are running "as Administrator" - so change the title and background color to indicate this
   $Host.UI.RawUI.WindowTitle = $myInvocation.MyCommand.Definition + "(Elevated)"
   #$Host.UI.RawUI.BackgroundColor = "DarkBlue"
   clear-host
   }
else
   {
   # We are not running "as Administrator" - so relaunch as administrator
   
   # Create a new process object that starts PowerShell
   $newProcess = new-object System.Diagnostics.ProcessStartInfo "PowerShell";
   
   # Specify the current script path and name as a parameter
   $newProcess.Arguments = $myInvocation.MyCommand.Definition;
   
   # Indicate that the process should be elevated
   $newProcess.Verb = "runas";
   
   # Start the new process
   [System.Diagnostics.Process]::Start($newProcess);
   
   # Exit from the current, unelevated, process
   exit
   }
### END ELEVATE TO ADMIN

import-module hyper-v

#Compact All VM HD's Where the VM is Off
$VMsThatAreOff = Get-VM | ?{$_.State -eq "Off"}
$VMsThatAreOn = Get-VM | ?{$_.State -eq "Running"}
$VMHDs = @()

Write-Host "The following VM's are off and ready to be compacted."
write-host ""
foreach($VM in $VMsThatAreOff)
{
    $Vm.Name
}
write-host ""
$CompactOffBoolean = Read-Host "Do you want to compact their disks? (Y/N)"



if ($CompactOffBoolean -eq "Y")
{
    foreach ($VM in $VMsThatAreOff)
    {
        $VMHDs += $VM | Get-VMHardDiskDrive
    }


    foreach ($HD in $VMHDs)
    {
        Optimize-VHD $HD.Path
    }
}



#Compact all VM's that are on, by saving, then compacting
$VMHDs = @()
Write-Host "We have found the following VM's that are currently running."
write-host ""
foreach($VM in $VMsThatAreOn)
{
    $Vm.Name
}
write-host ""
$SaveRunningBoolean = Read-Host "Do you want to save their state and compact their disks? (Y/N)"

if ($SaveRunningBoolean -eq "Y")
{
    foreach ($VM in $VMsThatAreOn)
    {
        $VM | Save-VM
    }

    foreach ($VM in $VMsThatAreOn)
    {
        $VMHDs += $VM | Get-VMHardDiskDrive
    }

    foreach ($HD in $VMHDs)
    {
        Optimize-VHD $HD.Path
    }

    foreach ($VM in $VMsThatAreOn)
    {
        $VM | Start-VM
        start-sleep -s 10
    }
}


remove-module hyper-v

